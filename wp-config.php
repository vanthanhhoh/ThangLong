<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thanglong');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bOq#{=YFDBJ>1,$>C0#V3Y&1CFz_Nh^yqkXiHI-?dfFoz+F(F&=GK:l`Goy-3g}5');
define('SECURE_AUTH_KEY',  'KuD!x=PgP;LDjN<=INb?`rneYMN7hezR@[>h1HD,tG.U~TOJqs7Z:4FCZDh/G7mr');
define('LOGGED_IN_KEY',    'wQ66iX.z h0$-Ql_m;fM+ZfrlRnkG8Vwo V(ufW|M#&.C{5&Nx[C/Th&~)tdDK&=');
define('NONCE_KEY',        '&;M,K8:</kE2cIrPaRw{_k|{A23hqZS{wl:KJ~|X&osH5M38yL)BY*hwYndMl~Hw');
define('AUTH_SALT',        ')1,IR>k^meQZ)E#?#lrDb?g&`p26]8J1@juwCrlICk)U7VGf[AVp9o0],H# N(_z');
define('SECURE_AUTH_SALT', 'GxT%:dJ~J. W+Oi^qJ(%cHrQ G=gDF;.nnf%IlHprZO0I}QwGO.Ow@)e|}d|P$w|');
define('LOGGED_IN_SALT',   'Zs:dB~5HTLA*,iNSKV#ld3j*}+O7~w)PBsnqT{f`+[M_-nd#Q|U7Sp}UGUX*LToY');
define('NONCE_SALT',       '&L.*qauwsHgbXX3<^L>ES7rF#y5l1~g8AEZ1i~z0)b->>E(c|@iIwD&zbbgl/*{7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

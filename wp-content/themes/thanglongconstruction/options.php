<?php
/**
 * Created by PhpStorm.
 * User: vanth
 * Date: 14/06/2017
 * Time: 11:42 SA
 */

Redux::setArgs(
    'thanglong', // This is your opt_name,
    array( // This is your arguments array
        'display_name' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'admin_bar' => 'true',
        'page_slug' => 'thanglong-options',
        'menu_type' => 'menu',
        'allow_sub_menu' => true,
    )
);

Redux::setSection(
    'thanglong',
    array(
        'id'    => 'header',
        'title' => 'Header',
        'icon' => 'el el-home',
        'fields' => array(
            array(
                'id'         => 'logo',
                'type'       => 'media',
                'title'      => 'Logo'
            ),
            array(
                'id'        => 'phone_bar',
                'type'      => 'text',
                'title'      => 'Hotline'
            ),
            array(
                'id'        => 'email_bar',
                'type'      => 'text',
                'title'      => 'Email'
            )
        )
    )
);
global $wpdb;
$post = $wpdb->get_results('SELECT * FROM `wp_posts` AS `post` LEFT JOIN `wp_term_relationships` AS `term` ON `post`.`ID` = `term`.`object_id` WHERE `term`.`term_taxonomy_id` IN (2,3)');

$options = array();
foreach ($post as $item){
    $options[$item->ID] = $item->post_title;
}
$slider = array();
$wp_slider = $wpdb->get_results('SELECT * FROM `wp_revslider_sliders`');
foreach ($wp_slider as $item){
    $slider[$item->alias] =$item->title;
}

Redux::setSection(
    'thanglong',
    array(
        'id'    => 'homepage',
        'title' => 'Homepage',
        'icon' => 'el el-puzzle',
        'fields' => array(
            array(
                'id'       => 'intro_text',
                'type'     => 'textarea',
                'title'    => __('Giới thiệu - Text', 'thanglong')
            ),
            array(
                'id'       => 'highlight',
                'type'     => 'select',
                'title'    => __('Giới thiệu - Nổi bật nhất', 'thanglong'),
                'options'  => $options
            ),
            array(
                'id'       => 'sub-highlight',
                'type'     => 'select',
                'multi'    => true,
                'title'    => __('Giới thiệu - Nổi bật', 'thanglong'),
                'options'  => $options
            ),
            array(
                'id'       => 'intro_slider',
                'type'     => 'select',
                'title'    => __('Giới thiệu - Slider', 'thanglong'),
                'options'  => $slider
            ),
            array(
                'id'       => 'duan_text',
                'type'     => 'textarea',
                'title'    => __('Dự án - Text', 'thanglong')
            ),
            array(
                'id'               => 'giaithuong-text',
                'type'             => 'editor',
                'title'            => __('Giải thưởng - Text', 'thanglong'),
                'args'   => array(
                    'teeny'            => true,
                    'textarea_rows'    => 10
                )
            ),
            array(
                'id'         => 'giaithuong-image',
                'type'       => 'media',
                'title'      => 'Giải thưởng - Image'
            ),
            array(
                'id'       => 'lydo_text',
                'type'     => 'textarea',
                'title'    => __('Vì sao chọn - Text', 'thanglong')
            ),
            array(
                'id'               => 'lydo_content',
                'type'             => 'editor',
                'title'            => __('Vì sao chọn - Content', 'thanglong'),
                'args'   => array(
                    'teeny'            => true,
                    'textarea_rows'    => 10
                )
            ),
            array(
                'id'               => 'hosonangluc',
                'type'             => 'text',
                'title'            => __('Hồ sơ năng lực', 'thanglong'),
            ),
            array(
                'id'               => 'danhsachkhachhang',
                'type'             => 'gallery',
                'title'            => __('Danh sách khách hàng','thanglong')     
            ),
            array(
                'id'               => 'congtythanhvien',
                'type'             => 'gallery',
                'title'            => __('Công ty thành viên','thanglong')     
            )
        )
    )
);

Redux::setSection(
    'thanglong',
    array(
        'id'    => 'nhanxet',
        'title' => 'Nhận Xét',
        'icon'  => 'el el-group-alt',
        'fields' => array(
            array(
                'id'       => 'nhanxetkhachhang',
                'type'     => 'slides',
                'title'    => __('Nhận Xét Khách Hàng', 'thanglong'),
                'placeholder' => array(
                    'title'           => __('Tên', 'thanglong'),
                    'description'     => __('Nhận xét', 'thanglong'),
                    'url'             => __('Chức vụ', 'thanglong')
                )
            )
        )
    ) 
);


Redux::setSection(
    'thanglong',
    array(
        'id'    => 'contact',
        'title' => 'Liên hệ',
        'icon'  => 'el el-comment',
        'fields' => array(
            array(
                'id'       => 'company_name',
                'type'     => 'text',
                'title'    => __('Tên công ty', 'thanglong')
            ),
            array(
                'id'       => 'company_address',
                'type'     => 'text',
                'title'    => __('Địa chỉ', 'thanglong')
            ),
            array(
                'id'       => 'company_email',
                'type'     => 'text',
                'title'    => __('Email', 'thanglong')
            ),
            array(
                'id'       => 'company_hotline',
                'type'     => 'text',
                'title'    => __('Hot line', 'thanglong')
            ),
            array(
                'id'       => 'company_phone',
                'type'     => 'text',
                'title'    => __('Phone', 'thanglong')
            ),
            array(
                'id'       => 'company_fax',
                'type'     => 'text',
                'title'    => __('Fax', 'thanglong')
            ),
            array(
                'id'       => 'company_map',
                'type'     => 'ace_editor',
                'mode'     => 'html',
                'theme'    => 'monokai',
                'title'    => __('Map', 'thanglong')
            )
        )
    ) 
);
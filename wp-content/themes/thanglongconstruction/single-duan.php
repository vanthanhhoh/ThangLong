<?php
get_header(); ?>
<link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/lib/lightgallery/css/lightgallery.css"/>
<div class="detail-duan">
	<div class="container">
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_title( '<h2 class="style_block_title text-center uppercase bold">', '</h2>' ); ?>
				<div class="gioithieuduan">
					<?php 
						the_content();
					?>
				</div>
				<div id="aniimated-thumbnials">
					<?php if(get_gallery()) :  ?>
					<?php foreach ( get_gallery() as $attachment ) : ?>
							
    						<a href="<?php echo $attachment->large_url ?>" class="gallery-item">
    							<img src="<?php echo get_image_thumnail($attachment->large_url,400,300) ?>" alt="<?php echo $attachment->alt ?>"/>
    							<div class="zoomp-images">
    								<i class="fa fa-search" aria-hidden="true"></i>
    							</div>
  							</a>
					<?php endforeach; ?>
					<?php endif; ?>	
				</div>
				<div id="share_post">
                                <div class="pull-left">
                                    <label>Chia sẻ</label>
                                </div>
                                <div class="pull-left">
                                    <div id="share">

                                    </div>
                                </div>
                        </div>
	        <?php endwhile; ?>
    	<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/lib/lightgallery/js/lightgallery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

<script type="text/javascript">
	(function($){
	    $('#aniimated-thumbnials').lightGallery({
		    thumbnail:true,
		    animateThumb: false,
		    showThumbByDefault: false
		}); 
	})(jQuery);
</script>
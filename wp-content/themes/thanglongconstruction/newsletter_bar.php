<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="download_ho_so_nang_luc">
				<label class="uppercase">
					<i class="fa fa-download" aria-hidden="true"></i> <a href="<?php echo $thanglong['hosonangluc'] ?>">Download hồ sơ năng lực</a>
				</label>
				<p class="description">
					Tóm tắt những thông tin và hình ảnh của công ty CPXD Thăng Long, giúp quý khách hàng tìm hiểu khái quát về công ty.
				</p>
			</div>
		</div>
		<div class="col-md-6">
			<div class="dangkinhantin">	
					<?php 
				if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 7 ); }
					?>
			</div>	
		</div>
	</div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: vanth
 * Date: 14/06/2017
 * Time: 10:29 SA
 */
?>

<?php get_header(); ?>

<?php if(is_home()) : ?>
    <div class="slider">
        <?php
            include_once "slider.php";
        ?>
    </div>
    <div class="gioithieu">
        <?php
        include_once "gioithieu.php";
        ?>
    </div>
    <div class="list-duan">
        <?php
        include_once "list-duan.php";
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="chungnhan-giaithuong">
        <?php
        include_once "chungnhan-giaithuong.php";
        ?>
    </div>

    <div class="taisao-chon">
        <?php
        include_once "lydo.php";
        ?>
    </div>
    <div class="newsletter_bar">
        <?php 
        include_once "newsletter_bar.php";
        ?>
    </div>
    <div class="khachhang">
        <?php 
        include_once "khachhang.php";
        ?>
    </div>
<?php endif; ?>

<?php get_footer(); ?>
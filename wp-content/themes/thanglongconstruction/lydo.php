<div class="container">
    <h2 class="style_block_title text-center uppercase white">
        TẠI SAO NÊN CHỌN <strong>THĂNG LONG JSC?</strong>
    </h2>
    <p class="introduction_text white text-center">
        <?php echo $thanglong['lydo_text'] ?>
    </p>
    <div class="lydo-content">
        <?php echo $thanglong['lydo_content'] ?>
    </div>
</div>
<div class="container">
    <h2 class="style_block_title text-center uppercase">
        <?php _e('Giới thiệu về <strong>Thăng long JSC</strong>') ?>
    </h2>
    <p class="introduction_text text-center">
        <?php echo $thanglong['intro_text'] ?>
    </p>
    <div class="list-feature">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="big-feature">
                    <?php putRevSlider($thanglong['intro_slider']); ?>
                    <?php $top_post = get_post($thanglong['highlight']) ?>
                    <p class="big-feature-description">
                        <?php echo wp_trim_words($top_post->post_excerpt,70,'...') ?>
                    </p>
                    <a class="uppercase bold readmore-full-width" href="<?php echo get_permalink($thanglong['highlight']) ?>" title="<?php echo $top_post->post_title ?>" class="readmore-full-width">
                        <?php _e('Đọc thêm') ?>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <?php
                    $args = array(
                        'post__in' => $thanglong['sub-highlight']
                    );
                    $posts = get_posts($args);
                    ?>
                    <?php foreach ($posts as $item) : ?>
                        <div class="col-md-6 col-sm-6">
                            <div class="feature-item">
                                <?php echo get_post_thumnail($item->ID,270,152); ?>
                                <div class="feature-item-title bold uppercase">
                                    <?php echo $item->post_title ?>
                                </div>
                                <p class="feature-item-description">
                                    <?php echo wp_trim_words($item->post_excerpt,15,'...<a href="'.get_permalink($item->ID).'" class="bold" title="'.$item->post_title.'">Đọc tiếp</a>') ?>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
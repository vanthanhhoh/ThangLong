<?php
/**
 * Created by PhpStorm.
 * User: vanth
 * Date: 14/06/2017
 * Time: 10:29 SA
 */

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'options.php';
add_action( 'after_setup_theme', 'setupTheme' );

function setupTheme(){
    load_theme_textdomain( 'thanglong' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );

    register_nav_menus( array(
        'primary' => __( 'Menu chính', 'thanglong' ),
        'social'  => __( 'Mạng xã hội','thanglong' ),
    ) );

    add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
    add_theme_support( 'customize-selective-refresh-widgets' );
}

add_action( 'widgets_init', 'thanglong_widget_init' );

function thanglong_widget_init(){
    register_sidebar( array(
        'name'          => __( 'Sidebar left', 'thanglong' ),
        'id'            => 'left',
        'description'   => __( 'Add widgets here to appear in your sidebar.', 'thanglong' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}

add_action( 'wp_enqueue_scripts', 'thanglong_scripts' );

function thanglong_scripts(){
    wp_enqueue_script( 'my-great-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.5', true );
    wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/isotope.min.js', array( 'jquery' ), '3.0.4', true );
    wp_enqueue_script( 'lightslider', get_template_directory_uri() . '/js/lightslider.min.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'jssocials', get_template_directory_uri() . '/js/jssocials.min.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '0.1.0', true );
}

function get_post_thumnail($id,$width,$height) {
    $imageUrl = get_the_post_thumbnail_url($id);
    $imageUrl = str_replace(get_site_url(),'',$imageUrl);
    $imageUrl = ltrim($imageUrl,'/');
    return '<img src="'.'/thumbnail/'.$width.'/'.$height.'/'.$imageUrl.'"/>';
}

function get_image_thumnail($image,$width,$height) {
    $imageUrl = str_replace(get_site_url(),'',$image);
    $imageUrl = ltrim($imageUrl,'/');
    return get_site_url().'/thumbnail/'.$width.'/'.$height.'/'.$imageUrl;
}

function page_permalink_by_slug($slug) {
  return get_permalink( get_page_by_path( $slug ) );
}
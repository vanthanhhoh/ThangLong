<?php
get_header(); ?>

<div class="archive_content">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="sidebar">
					<?php dynamic_sidebar('left'); ?>
				</div>
			</div>
			<div class="col-md-9">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_title( '<h1 class="entry-title title-style2 bold uppercase">', '</h1>' ); ?>
						<div class="single-content">
							<?php 
								the_content();
							 ?>
						</div>
						
					<?php endwhile; ?>
						<div id="share_post">
                                <div class="pull-left">
                                    <label>Chia sẻ</label>
                                </div>
                                <div class="pull-left">
                                    <div id="share">

                                    </div>
                                </div>
                        </div>
				<?php endif; ?>	
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<?php
get_header(); ?>

<div class="archive_content">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="sidebar">
					<?php dynamic_sidebar('left'); ?>
				</div>
			</div>
			<div class="col-md-9">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="row">
								<div class="col-md-5">
									<div class="article_thumb">
									    <a href="<?php echo esc_url( get_permalink() ) ?>" title="<?php the_title() ?>">
									    	<?php echo get_post_thumnail(get_the_ID(),370,208); ?>
									    </a>
									</div>
								</div>
								<div class="col-md-7">
									<div class="entry_content">
										<div class="entry-header">
										<?php
											if ( is_single() ) :
												the_title( '<h1 class="entry-title">', '</h1>' );
											else :
												the_title( sprintf( '<h2 class="entry-title bold uppercase"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
											endif;
										?>
									</div>
									<div class="entry-date">
										<?php the_date('d/m/Y', '<span><i class="fa fa-calendar" aria-hidden="true"></i>', '</span>'); ?>
									</div>
									<div class="entry-description">
										<?php 
											echo wp_trim_words(get_the_excerpt(),60,'..');
										?>
									</div>
									</div>
								</div>
							</div>
						</article>
					<?php endwhile; ?>
					<div class="pagging">
						<?php 
							the_posts_pagination( array(
								'prev_text'          => __( '<i class="fa fa-angle-left" aria-hidden="true"></i>', 'thanglong' ),
								'next_text'          => __( '<i class="fa fa-angle-right" aria-hidden="true"></i>', 'thanglong' )
							) );
						?>
					</div>
				<?php endif; ?>	
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>

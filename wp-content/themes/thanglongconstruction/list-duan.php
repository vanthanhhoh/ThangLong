<div class="container">
    <h2 class="style_block_title text-center uppercase white">
        <?php _e('Các dự án của <strong>Chúng tôi</strong>') ?>
    </h2>
    <p class="introduction_text white text-center">
        <?php echo $thanglong['duan_text'] ?>
    </p>
    <div class="list-khu-vuc">
        <ul class="text-center">
            <li class="active">
                <button class="uppercase filters" data-filter="*">Tất cả</button>
            </li>
            <?php
                $khuvuc = get_terms( array(
                    'taxonomy' => 'khuvuc_duan',
                    'hide_empty' => false,
                ));
            ?>
            <?php foreach ($khuvuc as $item) : ?>
                <li>
                    <button class="uppercase filters" data-filter=".<?php echo $item->slug ?>"><?php echo $item->name ?></button>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<div class="list-item-projects">
    <?php $args = array(
        'posts_per_page'   => 12,
        'offset'           => 0,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'duan',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );
    $posts_array = get_posts( $args );?>

    <div class="grid" id="grid">
        <?php foreach ($posts_array as $post) : ?>
            <?php $terms = wp_get_post_terms($post->ID,'khuvuc_duan'); $termsSlug=array(); foreach ($terms as $term) {$termsSlug[]=$term->slug; }?>
            <div class="element-item <?php echo implode(' ',$termsSlug) ?>" data-category="<?php echo implode(' ',$termsSlug) ?>">
                <?php echo get_post_thumnail($post->ID,390,219); ?>
                <div class="view-action">
                    <a href="<?php echo get_permalink($post->ID) ?>" class="view-album" data-id="<?php echo $post->ID ?>" title="<?php echo $post->post_title ?>">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                    <h3 class="project-title">
                        <?php echo $post->post_title ?>
                    </h3>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="readmore text-center">
    <a href="<?php echo page_permalink_by_slug('du-an') ?>" class="view-more-project bold">Xem tất cả</a>
</div>
<div class="container">
	<h2 class="style_block_title text-center uppercase">
        Khách hàng <strong>& Đối tác</strong>    
    </h2>
    <?php 
    	$clientsStr = $thanglong['danhsachkhachhang'];
    	$clients    = explode(',',$clientsStr);	
    ?>
    <div class="marquee-logo">
        <div id="list-khachhang">
        
            <?php foreach ($clients  as $key  => $logo) : ?>
                    <div class="logo-item">
                        <img src="<?php echo wp_get_attachment_url($logo) ?>" alt="Doi tac"/>
                    </div>
            <?php endforeach; ?>
            
        </div>  
    </div>
    <div class="nhanxet">	
    	<div class="row">
    		<div class="col-md-7">
    			<h3 class="title-style-3">
    				NHẬN XÉT CỦA <strong>KHÁCH HÀNG</strong>
    			</h3>
    			<div id="list-comment">
    				<?php foreach($thanglong['nhanxetkhachhang'] as $item): ?>
    					<div class="item-comment">
    						<div class="clientImage pull-left">
    							<img src="<?php echo wp_get_attachment_url($item['attachment_id']) ?>" alt="<?php echo $item['title'] ?>">
    						</div>
    						<div class="rightComment pull-left">
    							<div class="name_customer">
    								<?php echo '<span class="bold uppercase">'.$item['title'].'</span>'.' - '.$item['url'] ?>
    							</div>
    							<div class="comment_customer">
    								<?php echo $item['description'] ?>
    							</div>
    						</div>	
    					</div>	
    				<?php endforeach; ?>	
    			</div>
    		</div>
    		<div class="col-md-5">
    			<h3 class="title-style-3">
                    ĐƠN VỊ <strong>THÀNH VIÊN</strong>
                </h3>
                <div id="list-donvithanhvien">
                    <?php 
                        $logosArr = $thanglong['congtythanhvien'];
                        $logos    = explode(',',$logosArr);
                    ?>
                    <?php foreach ($logos as $key => $id) : ?>
                        <div class="item-donvithanhvien">
                            <img src="<?php echo wp_get_attachment_url($id) ?>"/>
                        </div>
                    <?php endforeach; ?>    
                </div>
    		</div>
    	</div>	
    </div>	
</div>
(function($){
    $(window).load(function() {
        var $grid = $('#grid').isotope({
            itemSelector: '.element-item',
            animationOptions: {
                filter: '*',
                easing: 'linear',
                queue: false
            }
        });
        $('button.filters').click(function () {
            var filterValue = $( this ).attr('data-filter');
            $grid.isotope({ filter: filterValue });
            $(this).parents('li').addClass('active');
            $(this).parents('li').siblings().removeClass('active');
        })
    });
    $('#list-comment').lightSlider({
            item:1,
            controls: false,
            pager : true
    });
    $('#list-donvithanhvien').lightSlider({
            item:2,
            controls: true,
            pager : false,
            prevHtml : '<div class="left-arrow arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
            nextHtml : '<div class="right-arrow arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>'
    });
    $('#list-khachhang').lightSlider({
            item:5,
            controls: false,
            pager : false,
            auto :true,
            pauseOnHover : true,
            loop : true,
            slideMargin : 0,
            responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                  }
            }
            ]
    });
    $("#share").jsSocials({
        shares: ["twitter", "facebook", "googleplus"],
        showLabel: true,
        showCount: true
    });
})(jQuery);
<?php
get_header(); ?>

<div class="list-du-an-page">
	<div class="container">
		<h2 class="style_block_title text-center uppercase">
        Các dự án của <strong>Chúng tôi</strong>    
        </h2>
        <div class="list-khu-vuc list-du-an-page-detail">
	        <ul class="text-center">
	            <li class="active">
	                <button class="uppercase filters" data-filter="*">Tất cả</button>
	            </li>
	            <?php
	                $khuvuc = get_terms( array(
	                    'taxonomy' => 'khuvuc_duan',
	                    'hide_empty' => false,
	                ));
	            ?>
	            <?php foreach ($khuvuc as $item) : ?>
	                <li>
	                    <button class="uppercase filters" data-filter=".<?php echo $item->slug ?>"><?php echo $item->name ?></button>
	                </li>
	            <?php endforeach; ?>
	        </ul>
    	</div>
    	<div class="list-item-projects">
    <?php $args = array(
        'offset'           => 0,
        'orderby'          => 'date',
        'order'            => 'DESC',
        'post_type'        => 'duan',
        'post_status'      => 'publish',
        'suppress_filters' => true
    );
    $posts_array = get_posts( $args );?>

    <div class="grid row" id="grid">
        <?php foreach ($posts_array as $post) : ?>
            <?php $terms = wp_get_post_terms($post->ID,'khuvuc_duan'); $termsSlug=array(); foreach ($terms as $term) {$termsSlug[]=$term->slug; }?>
            <div class="element-item col-md-4 <?php echo implode(' ',$termsSlug) ?>" data-category="<?php echo implode(' ',$termsSlug) ?>">
                
                <div class="inner" style="position: relative;">
                	<?php echo get_post_thumnail($post->ID,390,219); ?>
                	<div class="view-action">
                    <a href="<?php echo get_permalink($post->ID) ?>" class="view-album" data-id="<?php echo $post->ID ?>" title="<?php echo $post->post_title ?>">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                    <h3 class="project-title">
                        <?php echo $post->post_title ?>
                    </h3>
                </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
	</div>
</div>
<?php get_footer(); ?>
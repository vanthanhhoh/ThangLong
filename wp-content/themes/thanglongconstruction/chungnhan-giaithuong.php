<div class="container">
    <div class="row">
        <div class="col-md-7" id="text-certificate">
            <h2 class="title-style2 bold">
                Chứng nhận  / Giải Thưởng
            </h2>
            <div class="award-text">
                <?php echo $thanglong['giaithuong-text'] ?>
            </div>
        </div>
        <div class="col-md-5" id="image-certificate">
            <img src="<?php echo $thanglong['giaithuong-image']['url'] ?>" alt="certificate" width="100%">
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: vanth
 * Date: 14/06/2017
 * Time: 10:29 SA
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package ThangLong
 * @subpackage ThangLong
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.min.v3.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/fonts/myriad-pro/style.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/lightslider.min.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jssocial/jssocials-theme-flat.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jssocial/jssocials.css"/>
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css"/>
    <!--[if lt IE 9]>
        <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
    global $thanglong;
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5">
                <h1 class="title-page bold">
                    <?php print_r(__('Công ty Cổ phần Xây dựng Thăng Long','thanglong')) ?>
                </h1>
            </div>
            <div class="col-md-5 col-sm-4 text-center">
                <ul class="list-hot-contact">
                    <li>
                        <?php printf(__('<i class="fa fa-phone-square" aria-hidden="true"></i>
Hotline: %s','thanglong'),$thanglong['phone_bar']) ?>
                    </li>
                    <li>
                        <?php printf(__('<i class="fa fa-envelope" aria-hidden="true"></i>
Hotline: %s','thanglong'),$thanglong['email_bar']) ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3">
                <?php get_search_form() ?>
            </div>
        </div>
    </div>
</header>

<nav id="mainNav" class="navbar navbar-default <?php if(!is_home()) : ?>nothomepage<?php endif; ?>">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <img src="<?php _e($thanglong['logo']['url']) ?>" alt="Công Ty Cổ Phần Xây Dựng Thăng Long">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <?php wp_nav_menu(
            array(
                'menu'=>'primary',
                'container'=> 'div',
                'container_class'=>'collapse navbar-collapse',
                'container_id' => 'bs-example-navbar-collapse-1',
                'menu_class'      => 'nav navbar-nav',
                'menu_id'         => '',
                'depth'           => 1
            )
        ) ?>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<?php
if(!is_home()) {
if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<div id="breadcrumbs"><div class="container">','</div></div>
');
}
}
?>
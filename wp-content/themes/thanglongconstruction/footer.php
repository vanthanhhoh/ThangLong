<?php
    global $thanglong;
?>
<div class="contact-footer">
	<div class="container">
		<h2 class="style_block_title text-center uppercase white">
        	Liên hệ với <strong>Chúng tôi</strong>    
    	</h2>
    	<div class="contact_footer_content">
    		<div class="row">
    			<div class="col-md-6">
    				<div class="company_infomation">
    					<h3 class="company_name bold uppercase">
    						<?php echo $thanglong['company_name'] ?>
    					</h3>
    					<div class="address_list">
    						<div class="row">
    							<div class="col-md-6">
    								<div class="address_item">
    									<label>
    										Địa chỉ
    									</label>
    									<span>
    										<?php echo $thanglong['company_address'] ?>
    									</span>
    								</div>
    								<div class="address_item">
    									<label>
    										Email
    									</label>
    									<span>
    										<?php echo $thanglong['company_email'] ?>
    									</span>
    								</div>
    							</div>
    							<div class="col-md-6">
    								<div class="address_item">
    									<label>
    										Hotline
    									</label>
    									<span>
    										<?php echo $thanglong['company_hotline'] ?>
    									</span>
    								</div>
    								<div class="address_item">
    									<label>
    										Phone
    									</label>
    									<span>
    										<?php echo $thanglong['company_phone'] ?>
    									</span>
    								</div>
    								<div class="address_item">
    									<label>
    										Fax
    									</label>
    									<span>
    										<?php echo $thanglong['company_fax'] ?>
    									</span>
    								</div>
    							</div>
    						</div>
    					</div>

    					<div class="google-maps">
    						<?php echo $thanglong['company_map'] ?>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-6">
    				<h3 class="company_name bold uppercase">
    					Gửi tin nhắn cho chúng tôi
    				</h3>
    				<?php 	
    					if( function_exists( 'ninja_forms_display_form' ) )
                            { ninja_forms_display_form( 1 ); 
                        }
    				?>
    			</div>
    		</div>
    	</div>
	</div>
</div>
<div class="footerbar"> 
    <div class="container"> 
            <div class="row">   
                    <div class="col-md-8">
                        Copyright ©2017 by Thang Long Construction JSC. All Rights Reserved. Design by binhbt89@gmail.com
                    </div>
                    <div class="col-md-4">
                        <ul class="list_social_icon">   
                                <li>    
                                    <a href="" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                </li>
                                <li>    
                                    <a href="" class="googleplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </li>
                                <li>    
                                    <a href="" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </li>
                                <li>    
                                    <a href="" class="youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a>
                                </li>
                        </ul>   
                    </div>
            </div>  
    </div>  
</div> 
<?php wp_footer(); ?>
</body>
</html>

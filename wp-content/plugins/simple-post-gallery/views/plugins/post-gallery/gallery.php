<?php
/**
 * Gallery view/template.
 *
 * @author Alejandro Mostajo <http://about.me/amostajo>
 * @copyright 10Quality <http://www.10quality.com>
 * @package PostGallery
 * @version 1.0
 */ 
?>

<ul id="lightSlider">
    <?php foreach ( $post->gallery as $attachment ) : ?>
    <li data-thumb="<?php echo get_image_thumnail($attachment->large_url,300,150) ?>">
        <img src="<?php echo $attachment->large_url ?>" />
    </li>
    <?php endforeach ?>
</ul>
